<?php
    
    /**
        * Парсер PHP
        * Class Parser
    */
    require_once "vendor/autoload.php";
    //?PAGEN_2=922&bxajaxid=01cca19f151bba486835076528e20cd7
    
    $obj = New MarkiToArray('http://www.mnogomarok.ru/catalog/nepochtovye-marki/?PAGEN_1=');
    class MarkiToArray
    {
        function __construct($url) {
        //получаем урл
            $html = Parser::getPage([
            "url" => $url
            ]);
            
            if(!empty($html["data"])){
                $main = array();
                //получаем данные в html
                $xml = Parser::GetXml($html["data"]["content"]);
                //максимальнок число страниц
                $pm = MarkiToArray::PageMax($xml);
                //получаем в массив
                MarkiToArray::getArr($xml, $main);
                //$pm = 1;
                //echo $pm;
                echo (count($main));
                //цикл по страницам
                if(($pm !=0)&&($pm !=1))
                {
                    for($i = 2; $i<=$pm; $i++){
                        echo $i,"////////";
                        $html = Parser::getPage([
                        "url" => $url.$i
                        ]);
                        echo $url.$pm;
                        if(!empty($html["data"])){
                            $xml = Parser::GetXml($html["data"]["content"]);
                            MarkiToArray::getArr($xml, $main);
                            echo (count($main));
                        }
                    }
                }
                //создаем эксель
                Exel::Create($main);
            }
        }
        //массив марок
        public static function getArr($xml, &$main)
        {
            $images = $xml->xpath('//*[@class="products__item b-bg"]');
            $i = 0;
            foreach ($images as $image) {
                //var_dump($image->xpath('//*[contains(@class, "products__item-pic")]'));
                $pic = $image->xpath('//*[contains(@class, "products__item-pic")]');
                $link = $image->xpath('//*[contains(@class, "products__item-title")]//a');
                $price = $image->xpath('//*[contains(@class, "products__item-price")]//*[contains(@class, "pr")]');
                $main11= array('picture'=>$pic[$i]->img['src'][0], 'link'=>$link[$i]['href'], 'title'=>$link[$i]->__toString(), 'price'=>$price[$i]);
                array_push($main,$main11);
                $i++;
            }
        }
        //максимальное число страниц
        public static function PageMax($xml)
        {
            $pages = $xml->xpath('//*[@class="bx-pagination-container row"]');
            $pagemax =  $pages[0]->ul->li[count ($pages[0]->ul->li)-2]->a->span;
            return intval($pagemax);
        }
    }
    class Parser
    {
        /**
            * @var array
        */
        private static $arErrorCodes = [
        "CURLE_UNSUPPORTED_PROTOCOL",
        "CURLE_FAILED_INIT",
        "CURLE_URL_MALFORMAT",
        "CURLE_URL_MALFORMAT_USER",
        "CURLE_COULDNT_RESOLVE_PROXY",
        "CURLE_COULDNT_RESOLVE_HOST",
        "CURLE_COULDNT_CONNECT",
        "CURLE_FTP_WEIRD_SERVER_REPLY",
        "CURLE_REMOTE_ACCESS_DENIED",
        "CURLE_FTP_WEIRD_PASS_REPLY",
        "CURLE_FTP_WEIRD_PASV_REPLY",
        "CURLE_FTP_WEIRD_227_FORMAT",
        "CURLE_FTP_CANT_GET_HOST",
        "CURLE_FTP_COULDNT_SET_TYPE",
        "CURLE_PARTIAL_FILE",
        "CURLE_FTP_COULDNT_RETR_FILE",
        "CURLE_QUOTE_ERROR",
        "CURLE_HTTP_RETURNED_ERROR",
        "CURLE_WRITE_ERROR",
        "CURLE_UPLOAD_FAILED",
        "CURLE_READ_ERROR",
        "CURLE_OUT_OF_MEMORY",
        "CURLE_OPERATION_TIMEDOUT",
        "CURLE_FTP_PORT_FAILED",
        "CURLE_FTP_COULDNT_USE_REST",
        "CURLE_RANGE_ERROR",
        "CURLE_HTTP_POST_ERROR",
        "CURLE_SSL_CONNECT_ERROR",
        "CURLE_BAD_DOWNLOAD_RESUME",
        "CURLE_FILE_COULDNT_READ_FILE",
        "CURLE_LDAP_CANNOT_BIND",
        "CURLE_LDAP_SEARCH_FAILED",
        "CURLE_FUNCTION_NOT_FOUND",
        "CURLE_ABORTED_BY_CALLBACK",
        "CURLE_BAD_FUNCTION_ARGUMENT",
        "CURLE_INTERFACE_FAILED",
        "CURLE_TOO_MANY_REDIRECTS",
        "CURLE_UNKNOWN_TELNET_OPTION",
        "CURLE_TELNET_OPTION_SYNTAX",
        "CURLE_PEER_FAILED_VERIFICATION",
        "CURLE_GOT_NOTHING",
        "CURLE_SSL_ENGINE_NOTFOUND",
        "CURLE_SSL_ENGINE_SETFAILED",
        "CURLE_SEND_ERROR",
        "CURLE_RECV_ERROR",
        "CURLE_SSL_CERTPROBLEM",
        "CURLE_SSL_CIPHER",
        "CURLE_SSL_CACERT",
        "CURLE_BAD_CONTENT_ENCODING",
        "CURLE_LDAP_INVALID_URL",
        "CURLE_FILESIZE_EXCEEDED",
        "CURLE_USE_SSL_FAILED",
        "CURLE_SEND_FAIL_REWIND",
        "CURLE_SSL_ENGINE_INITFAILED",
        "CURLE_LOGIN_DENIED",
        "CURLE_TFTP_NOTFOUND",
        "CURLE_TFTP_PERM",
        "CURLE_REMOTE_DISK_FULL",
        "CURLE_TFTP_ILLEGAL",
        "CURLE_TFTP_UNKNOWNID",
        "CURLE_REMOTE_FILE_EXISTS",
        "CURLE_TFTP_NOSUCHUSER",
        "CURLE_CONV_FAILED",
        "CURLE_CONV_REQD",
        "CURLE_SSL_CACERT_BADFILE",
        "CURLE_REMOTE_FILE_NOT_FOUND",
        "CURLE_SSH",
        "CURLE_SSL_SHUTDOWN_FAILED",
        "CURLE_AGAIN",
        "CURLE_SSL_CRL_BADFILE",
        "CURLE_SSL_ISSUER_ERROR",
        "CURLE_FTP_PRET_FAILED",
        "CURLE_FTP_PRET_FAILED",
        "CURLE_RTSP_CSEQ_ERROR",
        "CURLE_RTSP_SESSION_ERROR",
        "CURLE_FTP_BAD_FILE_LIST",
        "CURLE_CHUNK_FAILED"
        ];
        
        /**
            * @param array $arParams
            * @return array|bool
        */
        public static function getPage($arParams = [])
        {
            if ($arParams) {
                if (!empty($arParams["url"])) {
                    $sUrl = $arParams["url"];
                    $sUserAgent = !empty($arParams["useragent"]) ? $arParams["useragent"] : "Mozilla/5.0 (Windows NT 6.3; W…) Gecko/20100101 Firefox/57.0";
                    $iTimeout = !empty($arParams["timeout"]) ? $arParams["timeout"] : 5;
                    $iConnectTimeout = !empty($arParams["connecttimeout"]) ? $arParams["connecttimeout"] : 5;
                    $bHead = !empty($arParams["head"]) ? $arParams["head"] : false;
                    $sCookieFile = !empty($arParams["cookie"]["file"]) ? $arParams["cookie"]["file"] : false;
                    $bCookieSession = !empty($arParams["cookie"]["session"]) ? $arParams["cookie"]["session"] : false;
                    $sProxyIp = !empty($arParams["proxy"]["ip"]) ? $arParams["proxy"]["ip"] : false;
                    $iProxyPort = !empty($arParams["proxy"]["port"]) ? $arParams["proxy"]["port"] : false;
                    $sProxyType = !empty($arParams["proxy"]["type"]) ? $arParams["proxy"]["type"] : false;
                    $arHeaders = !empty($arParams["headers"]) ? $arParams["headers"] : false;
                    $sPost = !empty($arParams["post"]) ? $arParams["post"] : false;
                    
                    if ($sCookieFile) {
                        file_put_contents(__DIR__ . "/" . $sCookieFile, "");
                    }
                    
                    $rCh = curl_init();
                    curl_setopt($rCh, CURLOPT_URL, $sUrl);
                    curl_setopt($rCh, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($rCh, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($rCh, CURLOPT_USERAGENT, $sUserAgent);
                    curl_setopt($rCh, CURLOPT_TIMEOUT, $iTimeout);
                    curl_setopt($rCh, CURLOPT_CONNECTTIMEOUT, $iConnectTimeout);
                    
                    if ($bHead) {
                        curl_setopt($rCh, CURLOPT_HEADER, true);
                        curl_setopt($rCh, CURLOPT_NOBODY, true);
                    }
                    
                    if (strpos($sUrl, "https") !== false) {
                        curl_setopt($rCh, CURLOPT_SSL_VERIFYHOST, true);
                        curl_setopt($rCh, CURLOPT_SSL_VERIFYPEER, true);
                    }
                    
                    if ($sCookieFile) {
                        curl_setopt($rCh, CURLOPT_COOKIEJAR, __DIR__ . "/" . $sCookieFile);
                        curl_setopt($rCh, CURLOPT_COOKIEFILE, __DIR__ . "/" . $sCookieFile);
                        
                        if ($bCookieSession) {
                            curl_setopt($rCh, CURLOPT_COOKIESESSION, true);
                        }
                    }
                    
                    if ($sProxyIp && $iProxyPort && $sProxyType) {
                        curl_setopt($rCh, CURLOPT_PROXY, $sProxyIp . ":" . $iProxyPort);
                        curl_setopt($rCh, CURLOPT_PROXYTYPE, $sProxyType);
                    }
                    
                    if ($arHeaders) {
                        curl_setopt($rCh, CURLOPT_HTTPHEADER, $arHeaders);
                    }
                    
                    if ($sPost) {
                        curl_setopt($rCh, CURLOPT_POSTFIELDS, $sPost);
                    }
                    
                    curl_setopt($rCh, CURLINFO_HEADER_OUT, true);
                    
                    $sContent = curl_exec($rCh);
                    $arInfo = curl_getinfo($rCh);
                    
                    $arError = false;
                    
                    if ($sContent === false) {
                        $arData = false;
                        
                        $arError["message"] = curl_error($rCh);
                        $arError["code"] = self::$arErrorCodes[curl_errno($rCh)];
                        } else {
                        $arData["content"] = $sContent;
                        $arData["info"] = $arInfo;
                    }
                    
                    curl_close($rCh);
                    
                    return [
                    "data" => $arData,
                    "error" => $arError
                    ];
                }
            }
            
            return false;
        }
        
        public static function GetXml($data)
        {
            $doc = new DOMDocument(); 
            $doc->loadHTML($data); 
            $xml = simplexml_import_dom($doc);
            return $xml;
        }
    }
    
    class Exel
    {
        public static function Create($data) {
            
            $document = new \PHPExcel();
            $objWriter = \PHPExcel_IOFactory::createWriter($document, 'Excel5');
            
            $objWriter->save("List.xls");
            
            $sheet = $document->setActiveSheetIndex(0); // Выбираем первый лист в документе
            
            $columnPosition = 0; // Начальная координата x
            $startLine = 2; // Начальная координата y
            
            // Вставляем заголовок в "A2" 
            $sheet->setCellValueByColumnAndRow($columnPosition, $startLine, 'Prices');
            
            // Выравниваем по центру
            $sheet->getStyleByColumnAndRow($columnPosition, $startLine)->getAlignment()->setHorizontal(
            PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            // Объединяем ячейки "A2:C2"
            $document->getActiveSheet()->mergeCellsByColumnAndRow($columnPosition, $startLine, $columnPosition+2, $startLine);
            
            // Перекидываем указатель на следующую строку
            $startLine++;
            
            // Массив с названиями столбцов
            $columns = ['N','Picture', 'Link', 'Title', 'Price'];
            
            // Указатель на первый столбец
            $currentColumn = $columnPosition;
            
            // Формируем шапку
            foreach ($columns as $column) {
                // Красим ячейку
                $sheet->getStyleByColumnAndRow($currentColumn, $startLine)
                ->getFill()
                ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('4dbf62');
                
                $sheet->setCellValueByColumnAndRow($currentColumn, $startLine, $column);
                
                // Смещаемся вправо
                $currentColumn++;
            }
            
            // Формируем список
            foreach ($data as $key=>$Item) {
                // Перекидываем указатель на следующую строку
                $startLine++;
                // Указатель на первый столбец
                $currentColumn = $columnPosition;
                // Вставляем порядковый номер
                $sheet->setCellValueByColumnAndRow($currentColumn, $startLine, $key+1);
                
                // Ставляем информацию об имени и цвете
                foreach ($Item as $value) {
                    $currentColumn++;
                    $sheet->setCellValueByColumnAndRow($currentColumn, $startLine, trim($value));
                }
            }
            
            $objWriter = \PHPExcel_IOFactory::createWriter($document, 'Excel5');
            $objWriter->save("List.xls");
        }
    }            