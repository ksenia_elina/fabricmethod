<?   
    namespace App;
    class MarkiToArray implements SettingParser 
    {
        public static function MainAction($url) {
        //получаем урл
            $html = Parser::getPage([
            "url" => $url
            ]);
            
            if(!empty($html["data"])){
                $main = array();
                //получаем данные в html
                $xml = Parser::GetXml($html["data"]["content"]);
                //максимальнок число страниц
                $pm = MarkiToArray::PageMax($xml);
                //получаем в массив
                MarkiToArray::getArr($xml, $main);
                echo (count($main));
                //цикл по страницам
                if(($pm !=0)&&($pm !=1))
                {
                    for($i = 2; $i<=$pm; $i++){
                        $html = Parser::getPage([
                        "url" => $url.$i
                        ]);
                        if(!empty($html["data"])){
                            $xml = Parser::GetXml($html["data"]["content"]);
                            MarkiToArray::getArr($xml, $main);
                            echo (count($main));
                        }
                    }
                }
                //создаем эксель
                Exel::Create($main);
            }
        }
        //массив марок
        public static function getArr($xml, &$main)
        {
            $images = $xml->xpath('//*[@class="products__item b-bg"]');
            $i = 0;
            foreach ($images as $image) {
                //var_dump($image->xpath('//*[contains(@class, "products__item-pic")]'));
                $pic = $image->xpath('//*[contains(@class, "products__item-pic")]');
                $link = $image->xpath('//*[contains(@class, "products__item-title")]//a');
                $price = $image->xpath('//*[contains(@class, "products__item-price")]//*[contains(@class, "pr")]');
                $main11= array('picture'=>$pic[$i]->img['src'][0], 'link'=>$link[$i]['href'], 'title'=>$link[$i]->__toString(), 'price'=>$price[$i]);
                array_push($main,$main11);
                $i++;
            }
        }
        //максимальное число страниц
        public static function PageMax($xml)
        {
            $pages = $xml->xpath('//*[@class="bx-pagination-container row"]');
            $pagemax =  $pages[0]->ul->li[count ($pages[0]->ul->li)-2]->a->span;
            return intval($pagemax);
        }
    }
?>