<?   
    namespace App;
    abstract class Manager
    {
        
        // Фабричный метод
        abstract public function makeParser():SettingParser;
        
        public function takeParse($url)
        {
            $SettingParser = $this->makeParser();
            $SettingParser->MainAction($url);
        }
    }
?>